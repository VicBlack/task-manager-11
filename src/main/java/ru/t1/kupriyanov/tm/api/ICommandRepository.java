package ru.t1.kupriyanov.tm.api;

import ru.t1.kupriyanov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
